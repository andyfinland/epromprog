/**
 * Render Serial connect
 * @param {*} reponse 
 */

function renderConnect(response) {

    hideMenu();

    $("#connect").text('Connect');
    $("#connect").parent().addClass('active');

    var response = ipcRenderer.sendSync('listPorts');

    var render = '<div id="connectSerial">';
    render += '<h1 class="centre-page"><small>Connect</small></h1>';
    render += '<div class="list-group">';
    render +='<a href="#" class="list-group-item list-group-item-action active list-header">Serial ports available</a>';

    for (i=0; i< response.length; i++) {
        render +='<a href="#" class="list-group-item list-group-item-action">'+response[i].path+'</a>';
    }

    render += '</div>';

    render += '<div class="remember"><input type="checkbox" class="form-check-input" id="remember">\
                <label class="form-check-label" for="remember">Remember this connection</label></div>'

    render += '</div>';

    $("#response").html(render);
}

