/**
 * Settings
 */

 /**
  * Render Settings
  */
 function renderSettings() {

    resetMenu();
    showMenu();
    $("#settings").parent().addClass('active');

    var render = '<h1><small>Settings</small></h1><div id="settingsResponse">\
                <form id="saveSettings">\
                <div class="form-group">\
                <label for="defaultDumpBytes">Default number of bytes from a ROM dump</label>\
                <input type="text" class="form-control" id="defaultDumpBytes" placeholder="Number" value="'+defaultDumpSize+'">\
                <label for="defaultEraseBytes">Default number of bytes to Erase</label>\
                <input type="text" class="form-control" id="defaultEraseBytes" placeholder="Number" value="'+defaultEraseSize+'">\
                <button type="submit" class="btn btn-primary">Update</button>\
                </div></form></div>';

    $("#response").html(render);
  
 }

 /**
  * Save default settings
  */
 function saveDefaultSettings() {
    localStorage.setItem('defaultDumpBytes', $("#defaultDumpBytes").val());
    localStorage.setItem('defaultEraseBytes', $("#defaultEraseBytes").val());
    //ipcRenderer.send('writeSerial','dump '+startAddress+' '+defaultDumpSize);
    writeCompletedMsg("Settings updated");
 }