 

/**
 * Custom erase
 */
function goErase() {

    var address = parseInt($("#eraseAddress").val(),16);
    var bytes = $("#eraseBytes").val();

    ipcRenderer.send('writeSerial','erase '+address+' '+bytes);
    
}

/**
 * Prompt Erase
 */
function promptErase() {

    resetMenu();
    showMenu();
    $("#erase").parent().addClass('active');

    var render = '<div id="erasePrompt">';
    
    render += '<h1><small>Are you sure you want to erase the ROM?</small></h1>';

    render += '<div id="eraseNav"> \
                <div class="eraseGroup input-group mb-3"> \
                    <div class="input-group-prepend"> \
                        <span class="input-group-text" id="inputGroup-sizing-default">Address</span> \
                    </div> \
                    <input class="form-control group-gap" name="address" id="eraseAddress" \
                    type="text" value="'+padHEX(startAddress,4)+'"/>';

        render +=  '<div class="input-group-prepend"> \
                        <span class="input-group-text" id="inputGroup-sizing-default">Bytes</span> \
                    </div> \
                    <input class="form-control" name="address" id="eraseBytes" type="text" value="'+defaultEraseSize+'"/> \
                    <button id="goErase" type="button" class="btn btn-primary">Erase</button> ';

    render += '</div>';
    

    render +=  '<div id="eraseProgress"><div class="progress" style="height: 20px;"> \
                    <div class="progress-bar" role="progressbar" style="width: 0%;" \
                        aria-valuenow="1" aria-valuemin="0" aria-valuemax="100"></div> \
                </div>';
  
    render += '</div>';
    render += '</div>';
  
    $("#response").html(render);
  
  }


/**
 * Check erase
 * @param {*} obj 
 */
function checkErase(obj) {

    if (obj.status == "progress") {

        if (obj.poll == 0) {
            console.log("Error: Mismatch on write", obj);
        }

        console.log(obj.status);
        processErase(obj);
    } else if (obj.status == "done") {
        eraseCompleted(obj);
    } else {
        console.log("Error: Erasing");
    }
}

/**
 * Process erase
 * @param {*} obj 
 */
function processErase(obj) {

    var total = obj.total;
    var bytes = obj.bytes;
    var part = obj.part;
    var inc = Math.round(bytes*part/total*100);
    if (inc>100) inc = 100;
    if (inc<0) inc = 0;

    $(".progress-bar").css("width", inc + "%").text(inc + " %");

}

 /**
 * Erase completed
 * @param {*} obj 
 */
function eraseCompleted(obj) {

    localStorage.removeItem('lastHexWrite');
    localStorage.removeItem('validateHEX');

    resetMenu();
    showMenu();
    $("#erase").parent().addClass('active');

    $("#eraseSpinner").remove();
    var render = '<div id="erasePrompt">';
  
    render += '<h1><small>Completed!</small></h1>';
    render += '<span class="material-icons mdc-text-lime-700 md-48">done</span>';
    render += "<h1><small>A total of "+obj.total+" bytes were erased<small></h1>"
  
    render += '</div>';
    $("#response").html(render);
  
}
