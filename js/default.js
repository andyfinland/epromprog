/**
 * Default JS
 */

/**
 * remove active from menu
 */
  function resetMenu() {
    $("#dump").parent().removeClass('active');
    $("#erase").parent().removeClass('active');
    $("#connect").parent().removeClass('active');
    $("#write").parent().removeClass('active');
    $("#settings").parent().removeClass('active');
  }

  /**
   * Hide menu
   */
  function hideMenu() {
    $("#dump").parent().hide();
    $("#erase").parent().hide();
    $("#write").parent().hide();
    $("#settings").parent().hide();
  }

  /**
   * show menu
   */
  function showMenu() {
    $("#dump").parent().show();
    $("#erase").parent().show();
    $("#write").parent().show();
    $("#settings").parent().show();
  }

  /**
 * Error
 */
function showError(msg='Oops something went wrong!') {

  hideMenu();

  var render = '<div id="errorMessage"><h1><small>'+msg+'</small></h1>\
              <span class="material-icons md-48 text-danger">error</span></div>';

  $("#response").html(render);
}

var timer;

/**
* waiting
*/
function showWaiting(msg='Just a sec ...') {

  var render = '<div id="waiting"><h1><small>'+msg+'</small></h1>\
                  <div class="spinner-border text-primary waiting" style="width: 2.5rem; height: 2.5rem;" role="status">\
                  <span class="sr-only">Loading...</span>\
                  </div>\
                </div>';
  $("#response").html(render);

  timer = setTimeout(connectTimeOut, 8000);
}

function connectTimeOut() {
  var render =  '<h2><small>Seems this is taking a while. Check your connection.</small></h2>\
                <button id="cancelWait" type="button" class="btn btn-primary">Cancel</button>';
  $(".waiting").after(render);
  clearTimeout(timer);
}

/**
 * Completed MEssage
 * @param {*} msg 
 */
function writeCompletedMsg(msg) {

  var render = '<div id="writeCompleted">';
    render += '<h1><small>'+msg+'</small></h1>';
    render += '<span class="material-icons mdc-text-lime-700 md-48">done</span>';
    render += '</div>';

    $("#response").html(render);

}

/**
 * Show ascii
 * @param {*} num 
 */
function renderAscii(num) {
  if (num>31 && num <127) {
    return String.fromCharCode(num);
  } else {
    return ".";
  }
}

/**
 * Pad HEX
 * @param {*} num 
 * @param {*} len 
 */
function padHEX(num, len) {

  if (num == null) {
    console.log("Error: Missing number");
    showError(msg='Oops something went wrong!');
  }

  var str = num.toString(16);
  var hex = "0".repeat(len - str.length) + str;
  return hex.toUpperCase();
}