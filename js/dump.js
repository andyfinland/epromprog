/**
 * Render Dump
 * @param {json} obj 
 */
function renderDump(obj) {

    resetMenu();
    showMenu();
    $("#dump").parent().addClass('active');
    

    var dataSize = obj.bytes;
    var cols = 16;
    var rows = dataSize/cols;
    var address = obj.address;

    // table header
    var render = '<h1><small>Dump</small></h1><div id="dumpResponse">';
    render += '<table class="table table-hover table-sm table-borderless table-light">';
    render += '<thead>';
    render += '<tr><th scope="col" class="mdc-bg-grey-100">Address</th>';
    render += '<th class="td-spacer mdc-bg-grey-100"></th>';
    for (var c=0; c<cols; c++) {
      render += '<th scope="col" class="mdc-bg-grey-100">'+padHEX(c,2)+'</th>';
    }
    render += '<th scope="col" class="td-spacer mdc-bg-grey-100">&nbsp;</th>';
    render += '<th scope="col" colspan="16" style="text-align: center" class="mdc-bg-grey-100 ascii-header">ASCII</th>';
    render += '</tr></thead><tbody>';

    // table contents
    for (var i=0; i<dataSize; i=i+cols) {
      
      render += '<tr>';
      render += '<th scope="row">'+padHEX(i+address,4)+'</th>';
      render += '<th class="td-spacer"></th>';

      // hex
      for (var h=0; h < cols; h++) {
        var dec = obj.data[i+h];
        render += '<td>'+padHEX(dec,2)+'</td>';
      }

      render += '<td class="td-spacer"></td>';

      // ascii
      for (var c=0; c<cols; c++) {
        var dec = obj.data[i+c];
        render += '<td class="ascii">'+renderAscii(dec)+'</td>';
      }

      render += '</tr>';
    }

    render += '</tbody></table>';
    render += '</div>';

        

    // nav
    render += '<div id="dumpNav"> \
                <div class="navButtons"> \
                  <button id="prevDump" type="button" class="btn btn-primary">Prev</button> \
                  <button id="nextDump" type="button" class="btn btn-primary">Next</button> \
                </div> \
                <div class="dumpGroup input-group mb-3"> \
                  <div class="input-group-prepend"> \
                    <span class="input-group-text" id="inputGroup-sizing-default">Address</span> \
                  </div> \
                  <input class="form-control group-gap" name="address" id="dumpAddress" \
                  type="text" value="'+padHEX(address,4)+'"/>';

        render += '<div class="input-group-prepend"> \
                    <span class="input-group-text" id="inputGroup-sizing-default">Bytes</span> \
                  </div> \
                  <input class="form-control" name="address" id="dumpBytes" type="text" value="'+defaultDumpSize+'"/> \
                  <button id="goDump" type="button" class="btn btn-primary">Go</button> ';

    render += '</div>';
    render += '</div>';
    
    $("#response").html(render);

}

/**
 * Custom dump
 */
function goDump() {
  var address = parseInt($("#dumpAddress").val(),16);
  var bytes = $("#dumpBytes").val();

  var byteInt = Number(bytes);

  // round up to nearest 16
  if (byteInt < 16) {
    bytes = '16';
  } else {
    byteInt = ((byteInt % 16==0) ? byteInt : byteInt-byteInt%16);
    bytes = byteInt.toString();
  }
  
  ipcRenderer.send('writeSerial','dump '+address+' '+bytes);
}

/**
 * Next dump
 */
function nextDump() {
  var address = parseInt($("#dumpAddress").val(),16);
  var bytes = parseInt($("#dumpBytes").val());
  var add = address+bytes;

  ipcRenderer.send('writeSerial','dump '+add+' '+bytes);
}

/**
 * Prev dump
 */
function prevDump() {

  var address = parseInt($("#dumpAddress").val(),16);
  var bytes = parseInt($("#dumpBytes").val());
  var add = address-bytes;
  if (address-bytes < 0) return
  ipcRenderer.send('writeSerial','dump '+add+' '+bytes);
}

