/**
 * Menu JS
 */

const {app, Menu, shell} = require('electron');
const defaultMenu = require('electron-default-menu');

const isMac = process.platform === 'darwin';
 
app.on('ready', () => {
 
  // Get template for default menu
  const menu = defaultMenu(app, shell);

  // Set top-level application menu, using modified template
  Menu.setApplicationMenu(Menu.buildFromTemplate(menu));
})