/**
 * Render Write
 */
function renderWrite() {

    resetMenu();
    showMenu();
    $("#write").parent().addClass('active');

    var render = '<h1><small>Write</small></h1><div id="writeResponse">';

    render +='<div id="hexArea">';
    render += '<div id="intelHex" contenteditable="true">';

    if (localStorage.getItem('lastHexWrite')== null || localStorage.getItem('lastHexWrite')=='') {
        render += 'Paste Intel HEX here';
    } else {
        let pretty = localStorage.getItem('lastHexWrite').replace(/:/g,String.fromCharCode(13)+String.fromCharCode(10)+":").trim();
        render += pretty;
    }
    
    render += '</div>';
    render += '</div>';

    render +='<button id="writeHEX" type="button" class="btn btn-primary">Write HEX</button>';

    if (localStorage.getItem('lastHexWrite')!= null && localStorage.getItem('lastHexWrite')!='') {
        render +='<button id="verifyHEX" type="button" class="btn btn-secondary">Verify HEX</button>';
    }
    render += '</div>';

    $("#response").html(render);

}



/**
 * Check write completed
 * @param {*} obj 
 */
function checkWrite(obj) {

    // send next line
    if (obj.status=='progress') {

    if (obj.poll == 0) {
        console.log("Error: Mismatch on write", obj);
    }
  
      var part = obj.part;
      sendIntelHEX(part+1);
  
    } else {
      
      // completed
      writeCompletedMsg('Intel HEX upload completed!');

      $("#writeHEX").show();
      $("#verifyHEX").show();
      
  
    }
}


/**
 * Start the Comparision process (round 1)
 * @param {*} index 
 */
function startComparision(index) {

    var arr = getDataByIndex(index);

    console.log("Type: ",arr[1]);

    // check type 0=data, 1=(:00000001FF)
    if (arr[1] == 0) {

        // still not end so get next hex line
        ipcRenderer.send('writeSerial','verify '+arr[3]+' '+arr[2]);

    } else {

        console.log("Comparison check completed");

        // start verification
        startVerfication();

    }

}

/**
 * Check for addresses that failed (round 2)
 */
function startVerfication() {

    var failed = localStorage.getItem('validateHEX');

    // if empty nothing failed
    if (failed == null || failed =="") {
       writeCompletedMsg("Verification Completed!");
    } else {
        failed = failed.slice(1);
        var res = failed.split(":");
        displayFailedAddresses(res);
    }

}

/**
 * Show failed addresses
 * @param {*} res 
 */
function displayFailedAddresses(res) {

    var render = '<div id="writeCompleted">';
    render += '<h1><small>Following addresses failed</small></h1>';
    render += '<span class="material-icons mdc-text-orange-700 md-48">warning</span>';
    render += '<div class="failedAddresses">';
    for (i=0; i<res.length; i++) {
        render +='<pre>'+res[i]+'</pre>';
    }
    render += '</div>';
    render += '</div>';

    $("#response").html(render);

}


/**
 * Verify HEX (incoming)
 * @param {*} obj 
 * [i, type, bytes, address, data, total]
 */
function verifyHEX(obj) {

    var bytes = obj.bytes;
    var address = obj.address;
    var data = obj.data;

    // find address
    var arr = getDataByAddress(address);

    // In first case display progress bar
    if (arr[0] == 0) {
        var render =  renderProgress("Performing a comparision check ...",0, 100);
        $("#writeHEX").hide();
        $("#verifyHEX").hide();
        $("#response").append(render);
    }

    // check verify data matches write data
    var s = localStorage.getItem("validateHEX");
    if (s==null) s="";

    if (arr[4] != convertDataToHEX(data)) {
        localStorage.setItem("validateHEX",s+":"+padHEX(address, 4));
    }

    var inc = Math.round((arr[0]+2)/arr[5]*100);
    $(".progress-bar").css("width", inc + "%").text(inc + " %");

    // jump to next address if not EOF
    startComparision(arr[0]+1);

}

/**
 * Convert bytes to hex
 * @param {arr} data 
 */
function convertDataToHEX(data) {
    var hex="";
    for (i=0; i<data.length; i++) {
        hex = hex + padHEX(data[i], 2);
    }
    return hex;
}

/**
 * Send HEX to ROM
 */
function sendIntelHEX(part) {
    
    var txt = $("#intelHex").text().trim();
    if (txt=="" || txt=="Paste Intel HEX here") return;

    // store hex in browser storage
    if (part == 1) {
        txt = txt.replace(/\r?\n|\r/g,'');
        localStorage.setItem('lastHexWrite',txt);
    }

    txt = txt.slice(1);

    var res = txt.split(":");
    if (res[res.length-1].trim() != '00000001FF') {
        console.log("Error: Missing EOF HEX");
        alert('Missing EOF HEX');
        return;
    }

    console.log(res[part-1].trim());
    ipcRenderer.send('writeSerial','write :'+ res[part-1].trim());

    // update progress bar
    if (part == 1) {
        var render =  renderProgress("Writing to ROM ...",0, 100);
        $("#writeHEX").hide();
        $("#verifyHEX").hide();
        $("#response").append(render);
    }

    var inc = Math.round(part/res.length*100);
    
    $(".progress-bar").css("width", inc + "%").text(inc + " %");
  
}



/**
 * Render progress
 * @param {} start 
 */
function renderProgress(msg="", start, max) {

    var render = '<div id="writeProgress">';
    if (msg!="") render += '<h2>'+msg+'</h2>';
    render += '<div class="progress" style="height: 20px;"> \
    <div class="progress-bar" role="progressbar" style="width: '+start+'%;" \
        aria-valuenow="'+start+'" aria-valuemin="0" aria-valuemax="'+max+'"></div> \
    </div></div>';

    return render;

}


/**
 * Look for address in last write
 * @param {} address 
 * @returns index, type, bytes, address, data array, total
 */
function getDataByAddress(findAddress) {

    console.log("Find address: ",findAddress);

    // read in last write from storage
    var txt = localStorage.getItem('lastHexWrite');
    txt = txt.slice(1);
    var res = txt.split(":");

    // look for address
    for (var i = 0; i<res.length; i++) {

        var address = parseInt(res[i].substring(2,6).trim(),16);

        if (findAddress==address) {

            var data = res[i].substring(8,res[i].length-2).trim();
            var bytes = data.length/2;
            var type = parseInt(res[i].substring(6,8),16);

            return [i, type, bytes, address, data, res.length];
        }

    }

    console.log("Error: Address not found");
    return null;

}   

/**
 * Get the data by index
 * @param {*} i 
 * @returns index, type, bytes, address, data array, total
 */
function getDataByIndex(i) {

    console.log("Find index: ",i);

    // read in last write from storage
    var txt = localStorage.getItem('lastHexWrite');
    txt = txt.slice(1);
    var res = txt.split(":");

    var address = parseInt(res[i].substring(2,6).trim(),16);
    var data = res[i].substring(8,res[i].length-2).trim();
    var bytes = data.length/2;
    var type = parseInt(res[i].substring(6,8),16);

    return [i, type, bytes, address, data, res.length];

}

