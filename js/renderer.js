/**
 * Renderer JS
 */

var $ = require('jquery');
require('popper.js');
require('bootstrap');
const {ipcRenderer} = require('electron');

var defaultDumpSize = 192;
var defaultEraseSize = 512;
var startAddress = 0;

/**
 * Init
 */

if (localStorage.getItem('defaultDumpBytes') != null) {
    defaultDumpSize = localStorage.getItem('defaultDumpBytes');
}

if (localStorage.getItem('defaultEraseBytes') != null) {
    defaultEraseSize = localStorage.getItem('defaultEraseBytes');
}

 let rem = localStorage.getItem('defaultEpromConnection');
 
 if (rem == null) {
    renderConnect();
 } else {
    ipcRenderer.send('openConnection',rem);
 }


/**
 * Open Serial port
 */
ipcRenderer.on('openSerialPort', (event, arg) => {

    if (arg.toString().search('Cannot lock port')>0) {

        console.log("Error: Serial port already open");
        showError();
        
    } else if (arg.toString().search('Error')>0) {

        console.log("Error: Opening serial port");
        showError();
        
    } else {
        showWaiting('Connecting ...');
        console.log("Serial connection open");
        ipcRenderer.send('initData',1);
        $("#connect").text('Disconnect');
    }
});

// handle close
ipcRenderer.on('isClosed', (event, arg) => {

    if (arg==1) {

        console.log("Serial connection closed");
        renderConnect();

    } else {
        
        console.log("Error: Closing connection", arg);
        showError();
        //ipcRenderer.send('restart');
        
    }
});


// handle incoming serial data
ipcRenderer.on('data', (event, arg) => {

    console.log(arg);

    if (arg == "READY") {

        console.log("Device ready");
        ipcRenderer.send('writeSerial','dump '+startAddress+' '+defaultDumpSize);

    } else {

        var obj = JSON.parse(arg);

        if (obj.action == 'dump') {
            renderDump(obj);
        }

        if (obj.action == 'verify') {
            verifyHEX(obj);
        }

        if (obj.action == 'write') {
            checkWrite(obj);
        }

        if (obj.action == 'erase') {
            checkErase(obj);
        }
    }
});

/**
 * Events
 */

$(document).on('click', '#connect', function(event) { 
    
    $('.navbar-collapse').collapse('hide');

    if ($("#connect").text() == 'Disconnect') {
        ipcRenderer.send('closeConnection');
    }
});

$(document).on('click', '#connectSerial>div>a', function(event) {

    var sel = event.target.text.trim();

    if (sel != 'Serial ports available')  {

        if ($('#remember').is(":checked")) {
            localStorage.setItem('defaultEpromConnection', sel);
        } else {
            localStorage.removeItem('defaultEpromConnection');
        }
        
        showWaiting('Connecting ...');
        ipcRenderer.send('openConnection',sel);
    }
});

$(document).on('click', 'html', function(event) {  
    $('.navbar-collapse').collapse('hide');  
});

$(document).on('click', '#dump', function(event) {  
    $('.navbar-collapse').collapse('hide');  
    ipcRenderer.send('writeSerial','dump '+startAddress+' '+defaultDumpSize);
});

$(document).on('click', '#goDump', function(event) {       
    goDump();
});

$(document).on('click', '#nextDump', function(event) {       
    nextDump();
  });

$(document).on('click', '#prevDump', function(event) {       
    prevDump();
});

$(document).on('click', '#erase', function(event) {  
    $('.navbar-collapse').collapse('hide');  
    promptErase();
});

$(document).on('click', '#write', function(event) {  
    $('.navbar-collapse').collapse('hide');  
    renderWrite();
});

$(document).on('click', '#writeHEX', function(event) {      
    sendIntelHEX(1);
});

$(document).on('click', '#verifyHEX', function(event) {      
    startComparision(0);
});

$(document).on('click', '#intelHex', function(event) {      
    if ($("#intelHex").text()=='Paste Intel HEX here') {
        $("#intelHex").text('');
    }
});

$(document).on('click', '#goErase', function(event) {      
    goErase();
});

$(document).on('click', '#settings', function(event) {      
    renderSettings();
});

$(document).on('click', '#cancelWait', function(event) {      
    localStorage.removeItem('defaultEpromConnection');
    ipcRenderer.send('closeConnection');
});

$(document).on('submit', '#saveSettings', function(event) {   
    event.preventDefault();   
    saveDefaultSettings();
});





