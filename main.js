/**
 * Main JS
 */

const {app, BrowserWindow, Menu} = require('electron');
const {ipcMain} = require('electron');
const path = require('path');
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');

//var readyParser;
var parser
var port;

// Menu
require('./js/template.js');

function createWindow () {
  
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 1280,
    height: 750,
    icon: 'build/awe.png',
    webPreferences: {
      nodeIntegration: true,
      allowRendererProcessReuse: false
    }
  })

  // Load html
  mainWindow.loadFile('index.html')

  // DevTools.
  //mainWindow.webContents.openDevTools()

}



// ready
app.whenReady().then(() => {

  createWindow()
  app.on('activate', function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })

})

// close
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

/**
 * IPC Main: List Serial Ports
 */
ipcMain.on('listPorts', (event,arg) => {
  SerialPort.list().then(function (data) {
    event.returnValue = data; 
  });
});

/**
 * Open Serial Connection
 */
ipcMain.on('openConnection', (event,arg) => {

  port = new SerialPort(arg, {
    baudRate: 57600, databits: 8, stopbits: 1, parity: 'none', autoOpen: false,
  });

  parser = port.pipe(new Readline({ delimiter: "\r\n", encoding: 'ascii'}))

  port.open(function (err) {
    
    if (err) {
      console.log(err);
      event.sender.send('openSerialPort',err);  
    }
    event.sender.send('openSerialPort',port.path);
  });
});

/**
 * Close Serial connection
 */
ipcMain.on('closeConnection', (event,arg) => {

  port.close(function (err) {
    if (err) {
      event.sender.send('isClosed',0);
    } else {
      event.sender.send('isClosed',1);
    }
  })

});

/**
 * Wait for serial data
 */
ipcMain.on('initData', (event,arg) => {

  parser.on('data', (data) => {
      console.log(data);
      event.sender.send('data',data);
  });

});

/**
 * Write to Serial port
 */
ipcMain.on('writeSerial', (event,arg) => {
  console.log(arg);
  port.write(arg+'\r\n');
});

/**
 * Restart
 */
ipcMain.on('restart', (event,arg) => {
  //app.relaunch();
  //app.exit(); 
  mainWindow.reload();
});